var bulan = [{
				nama:'Januari', 
				angka:1
			}, 
			{
				nama:'Pebruari', 
				angka:2
			}, 
			{
				nama:'Maret', 
				angka:3
			}, 
			{
				nama:'April', 
				angka:4}, 
			{
				nama:'Mei', 
				angka:5
			}, 
			{
				nama:'Juni', 
				angka:6
			}, 
			{
				nama:'Juli', 
				angka:7
			}, 
			{
				nama:'Agustus', 
				angka:8
			}, 
			{
				nama:'September', 
				angka:9
			}, 
			{
				nama:'Oktober', 
				angka:10
			}, 
			{
				nama:'November', 
				angka:11
			}, 
			{
				nama:'Desember', 
				angka:12
			}]

function getTahun(){
	for(var i=1980; i<=2000; i++){
		var newOption = document.createElement('option')
		newOption.value = i
		newOption.id = i
		newOption.text = i
		document.getElementById('tahun').append(newOption);
	}
}

function getBulan(){
	for (var i = 0; i < bulan.length; i++) {
	    var newOption = document.createElement('option');
	   newOption.value = bulan[i].angka;
	   newOption.text = bulan[i].nama;
	   document.getElementById('bulan').append(newOption);
	}
}

function tanggal(hari){
	for (var j = 1; j <= hari; j++) {
		var newOption = document.createElement('option')
		newOption.value = j
		newOption.text = j
		document.getElementById("tanggal").append(newOption);
	}
}

var klikTanggal = document.getElementById("tanggal")
var klikBulan = document.getElementById("bulan")
var klikTahun = document.getElementById("tahun")

function keong(){
	var bul = document.getElementById("bulan").value;
	var ta = document.getElementById("tahun").value;
	document.getElementById("tanggal").innerHTML = '';
	
	if (bul < 8) {
		if (bul % 2 == 1){
			tanggal(31);
		}
		else if(bul % 2 == 0 && bul != 2){
			tanggal(30);
		}
		else if(bul == 2 && ta % 4 != 0){
			tanggal(28);
		}
		else if(bul == 2 && ta % 4 == 0){
			tanggal(29);
		}
	}
	else{
		if (bul % 2 == 0){
			tanggal(31);
		}
		else if(bul % 2 == 1){
			tanggal(30);
		}
	}
}

getBulan();
getTahun();
klikTanggal.append(keong());

klikBulan.onclick = function(){
	keong();
}

klikTahun.onclick = function(){
	keong();
}

function cekKosong(parameter, pesan){
    if (parameter == 0) {
        document.getElementById(pesan).innerHTML = 'field tidak boleh kosong';
    }
}

function cekKarakter(parameter, pesan){
	if (parameter.length == 1 || parameter.length >= 21){
		document.getElementById(pesan).innerHTML = 'Nama tidak boleh kurang dari 2 dan lebih dari 20 huruf !!!';
	}
}

function cekSimbol(parameter, pesan) {   
	var letters = /^[A-Za-z]+$/;  
	if(parameter.match(letters)) {  
	return true;  
	} else {  
	document.getElementById(pesan).innerHTML = 'Nama tidak boleh diisi simbol atau angka !!!';   
	return false;  
	}  
}  

function cekValid(){
	var yangDiValidasi = [{
						    input:"depan",
						    error:"errorNamadepan"
						},{
						    input:"belakang",
						    error:"errorNamabelakang"
						}];
	for (var i in yangDiValidasi){
		var inputnya = document.getElementById(yangDiValidasi[i].input).value;
		var errornya = yangDiValidasi[i].error;
		document.getElementById(errornya).innerHTML = "";

   		cekKarakter(inputnya, errornya);
    	cekSimbol(inputnya, errornya);
    	cekKosong(inputnya, errornya);
    }
}

// function cekValid(){
//     for (var i in yangAkanDiValidasi) {
//         cekKosong(yangAkanDiValidasi[i])
//     }
// }
// function cekKosong(parameter){
//     var input = document.getElementById(parameter.input).value;
//     var errorArea = document.getElementById(parameter.error);
//     if (input.length == 0){
//         errorArea.innerHTML = 'Maaf, field ini tidak boleh kosong';
//         console.log("salah gan");
//     }
//     else{
//         errorArea.innerHTML = ' ';
//         console.log('cek field kosong lolos, lanjut gan');
//     }
// }

    // var atpos = emailInput.indexOf("@");
    // var dotpos = emailInput.lastIndexOf(".");
     // else if ((atpos < 1) || (dotpos - atpos < 2)) {
     // document.getElementById("errorEmail").innerHTML = "Email Salah";
     // }
     
function validateEmail(){
    var emailInput = document.getElementById("email").value;
 	var validate = /^([A-Za-z0-9_\-\.]){1,}\@([A-Za-z0-9_\-\.]){1,}\.([A-Za-z]{2,4})$/;

    if (validate.test(emailInput) == false) {
    document.getElementById("errorEmail").innerHTML = "Masukkan email yang bener Gan";
    document.getElementById("email").focus();
    } 
  }

document.getElementById("tombol").onclick=function(){
	var depan = document.getElementById("depan").value
	var belakang = document.getElementById("belakang").value
	var newOption = document.createElement('option')
	var i = document.getElementById("bulan").value

	cekValid();
	validateEmail();
	
	document.getElementById("output").innerHTML = 
		'Nama Anda : '  + depan + ' ' + belakang + '<br>' +
		'Tanggal Lahir : ' + klikTanggal.value +' ' + bulan[i-1].nama + ' ' + klikTahun.value;
}



// console.log("Script Running Well");

// var namaDepan = "wahyudy";
// var namaBelakang = "caesar";
// console.log(namaDepan, namaBelakang);

// var tahunIni = 2017.25;
// var tahunLahir = 1989;
// var x = namaDepan.length;

// var umur = tahunIni - tahunLahir;
// console.log(umur);

// document.write("Text ini tidak pernah diketik di HTML");
// document.write("Text ini tidak pernah diketik di HTML");
// document.write("Text ini tidak pernah diketik di HTML");
// document.write("Text ini tidak pernah diketik di HTML");

// //document.write untuk menulis didalam tag body
// // append untuk menambahkan,,innerHTML untuk mereplace

// document.getElementById("output").innerHTML='File ini tidak pernah diketik di HTML';
// document.getElementById("output").innerHTML='File ini tidak pernah diketik di HTML';
// document.getElementById("output").innerHTML=' ini tidak pernah diketik di HTML';
// document.getElementById("output").append(' ini tidak pernah diketik di HTML');
// document.getElementById("output").append(' ini tidak pernah diketik di L');
// var x = namaDepan.length;
// document.getElementById("output").innerHTML = x;

// console.log(typeof(namaDepan));

// function coba(){
// 	console.log("apaluuu");
// }
// coba();

// function tampilinTextPertama(cumi){
// 	document.getElementById('output').innerHTML=cumi;
// }

// tampilinTextPertama("coba");

// function Bulan(){
//     var x=['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'];
//     for (var i=0; i<=11; i++) {
//         var newOption = document.createElement('option')
//         newOption.value = i
//         newOption.text = x[i]
//         document.getElementById('bulan').append(newOption)
//     }
// }
// Bulan();






